#Write a python script to get a number N from user then create 3D array N x N x N
# with random (float) values on interval [0,10].
#Find min, max and sum of array by 3 axis (i.e., deep, height and width)

#---------------

# import library
import random
import numpy as np

# Ask user a number
number = int(input("Input a number: "))

# Create 3D array size = input number with random number from 1 to 10
array = 10*np.random.random((number,number, number))

# Find min, max, sum of deep, height, and width
#deep
deep = array.sum(axis = 0)
deep.sum()
deep.max()

#height
height = array.sum(axis = 1)
height.sum()
height.max()

#width
width = array.sum(axis = 1)
width.sum()
width.max()
